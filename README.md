# What to do

1. There's a class called UAWSimpleDataObject that needs test coverage for all the existing methods. All test should pass and the API should respect the comments. You have to figure out which
type of test is more convenient for this (FunctionalTest or UnitTest).
2. You have to create a FunctionalTest parametrized that verifies that the max walk speed of the character movement component effectivelly makes the character reach that velocity eventually.
To do so, there's a function that is implemented in blueprint (but can be called from C++) called "*StartWalkingForward*". You have to make this work with at least 300, 600 and 900 as max speed.