// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AWSimpleDataObject.generated.h"

/**
 * 
 */
UCLASS()
class UEAUTOMATIONWORKSHOP_API UAWSimpleDataObject : public UObject
{
	GENERATED_BODY()

public:

	/** Reset every member to:
	* StringData = ""
	* FloatData = 0.0f
	* Int32Data = 0
	* unless bIgnore is true, which does nothing. */
	virtual void ResetAttributes(bool bIgnore = false);

	/** Retrieve string data. */
	const FString& GetStringData() const;
	/** Retrieve float data. */
	float GetFloatData() const;
	/** Retrieve int data. */
	int32 GetInt32Data() const;

private:

	FString StringData = TEXT("MyString");
	float FloatData = 5.5f;
	int32 Int32Data = 156;
	
};
