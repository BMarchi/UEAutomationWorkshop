// Copyright Epic Games, Inc. All Rights Reserved.

#include "UEAutomationWorkshopGameMode.h"
#include "UEAutomationWorkshopCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUEAutomationWorkshopGameMode::AUEAutomationWorkshopGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
